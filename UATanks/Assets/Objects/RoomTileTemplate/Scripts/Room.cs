﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Room : MonoBehaviour {

    // This bool is used to mark some tiles as outdoor, which has different rules for map creation.
    // To wit: outdoor tiles do not have walls between other tiles, only with the edge of the map.
    // Indoor tiles have walls, but must have at least one connection to an outdoor tile if there is one neighboring.
    public bool isOutdoor;

    [Header("Half Walls - For Indoor Use Only")]
    // Half walls are placed when there is a connection between Indoor rooms and the outside, and between neighboring indoor rooms.
    public GameObject halfWallNorthEast;
    public GameObject halfWallNorthWest;
    public GameObject halfWallSouthEast;
    public GameObject halfWallSouthWest;
    public GameObject halfWallEastNorth;
    public GameObject halfWallEastSouth;
    public GameObject halfWallWestNorth;
    public GameObject halfWallWestSouth;

    [Header("Walls")]
    //Regular walls are placed when there is no connection between an indoor tile and a neighboring tile, as well as when the neighboring tile does not exist.
    public GameObject wallNorth;
    public GameObject wallSouth;
    public GameObject wallEast;
    public GameObject wallWest;

    [Header("Spawn Points")]
    // These spawn points will be passed to the game manager for its list.
    public List<Transform> enemySpawns;
    public Transform playerSpawn;

    [Header("Connections")]
    // For the map creation, we use these to check for connections between tiles, to make sure no tile has no connections, and to make sure not to put a wall where another tile had a door.
    public bool connectionNorth;
    public bool connectionSouth;
    public bool connectionEast;
    public bool connectionWest;

    [Header("Patrol Points")]
    public List<Transform> patrolPoints;

    [Header("Guard Post")]
    public Transform guardPost;
}
