﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class PowerupJoker : Powerup {

    private float oldForwardSpeed;
    private float oldReverseSpeed;

    public PowerupJoker (PowerupJoker old) : base(old) {

    }

    public override void OnAddPowerup ( TankData data ) {
        oldForwardSpeed = data.forwardSpeed;
        oldReverseSpeed = data.reverseSpeed;
        data.forwardSpeed = 0;
        data.reverseSpeed = 0;
        base.OnAddPowerup(data);
    }

    public override void OnRemovePowerup ( TankData data ) {
        data.forwardSpeed += oldForwardSpeed;
        data.reverseSpeed += oldReverseSpeed;
        base.OnRemovePowerup(data);
    }
}
