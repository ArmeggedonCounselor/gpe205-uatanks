﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PickupHealth : Pickup {

    public PowerupHealth powerup;

    public override void OnTriggerEnter ( Collider other ) {
        if (powerup.isPermanent) {
            TankHealth health = other.GetComponent<TankHealth>();
            if (health != null) {
                if (health.healthRatio < 1.0) {
                    PowerupManager powMan = other.GetComponent<PowerupManager>();
                    if (powMan != null) {
                        // Use the copy constructor here so that it's an entirely new copy of the original powerup.
                        powMan.AddPowerup(new PowerupHealth(powerup));
                        base.OnTriggerEnter(other);
                    }
                }
                else {
                    return;
                }
            }
            else {
                return;
            }
        }
        else {
            PowerupManager powMan = other.GetComponent<PowerupManager>();
            if (powMan != null) {
                // Use the copy constructor here so that it's an entirely new copy of the original powerup.
                powMan.AddPowerup(new PowerupHealth(powerup));
                base.OnTriggerEnter(other);
            }
        }
    }
}

