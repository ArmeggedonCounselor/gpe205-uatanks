﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[System.Serializable]
public class PowerupHealth : Powerup {

    public PowerupHealth ( PowerupHealth old ) : base(old) {

    }

    public override void OnAddPowerup (TankData data) {
        if (isPermanent) {
            if ((data.health.maxHealth - data.health.currentHealth) <= bonusAmount) {
                data.health.Heal(data.health.maxHealth - data.health.currentHealth);
            }
            else {
                data.health.Heal(bonusAmount);
            }
        }
        else {
            data.health.Heal(bonusAmount);
        }
        base.OnAddPowerup( data);
    }

    public override void OnRemovePowerup (TankData data) {
        
        data.health.TakeDamage(bonusAmount, null);
        base.OnRemovePowerup(data);
    }
}
