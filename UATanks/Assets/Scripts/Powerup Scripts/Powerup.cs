﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[System.Serializable]
public class Powerup {
    // Might make this into an abstract class and just include relevant methods.
    // For future consideration.
    
    // Powerup Data
    public float bonusAmount;
    public float countdown;
    public bool isPermanent;

    public Powerup( Powerup old ) {
        bonusAmount = old.bonusAmount;
        countdown = old.countdown;
        isPermanent = old.isPermanent;
    }

    public virtual void OnAddPowerup (TankData data) {

    }

    public virtual void OnRemovePowerup(TankData data) {

    }
}
