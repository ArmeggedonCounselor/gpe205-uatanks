﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[System.Serializable]
public class PowerupSpeed : Powerup {

	public PowerupSpeed (PowerupSpeed old) : base(old) {

    }

    public override void OnAddPowerup ( TankData data ) {
        data.forwardSpeed *= bonusAmount;
        data.reverseSpeed *= bonusAmount;
        base.OnAddPowerup(data);
    }

    public override void OnRemovePowerup ( TankData data ) {
        data.forwardSpeed /= bonusAmount;
        data.reverseSpeed /= bonusAmount;
        base.OnRemovePowerup(data);
    }
}
