﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnerPickup : MonoBehaviour {

    public GameObject pickupPrefab;
    private GameObject spawnedPickup;
    public float spawnDelay;
    public ParticleSystem glowyBits;
    private float spawnTimer;
    private Transform tf;


	// Use this for initialization
	void Start () {
        spawnTimer = 0.0f;
        spawnedPickup = null;
        tf = GetComponent<Transform>();
	}
	
	// Update is called once per frame
	void Update () {
		if(spawnTimer <= 0.0f) {
            spawnedPickup = Instantiate(pickupPrefab, tf.position, Quaternion.identity) as GameObject;
            glowyBits.Play();
            GameManager.gm.AddPowerup(spawnedPickup);
            spawnTimer = spawnDelay;
        }
        if (spawnedPickup == null) {
            if (glowyBits.isPlaying) {
                glowyBits.Stop();
            }
            spawnTimer -= Time.deltaTime;
        }
	}
}
