﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pickup : MonoBehaviour {
    
    public Transform tf;
    public float rotateSpeed;
    public float maximumVerticalDisplacement;
    public float timeBetweenPeaks = 3.0f;
    public bool upOrDown = true; // True is up, false is down
    private Vector3 startingPosition;
    private Vector3 endingPosition;
    private float elapsedTime = 0.0f;

    void Start () {
        tf = GetComponent<Transform>();
        startingPosition = tf.position;
        endingPosition = new Vector3(tf.position.x, tf.position.y + maximumVerticalDisplacement, tf.position.z);
    }

    void Update () {
        tf.Rotate(0, rotateSpeed * Time.deltaTime, 0);
        
        if (upOrDown) {
            elapsedTime += Time.deltaTime;
        } else {
            elapsedTime -= Time.deltaTime;
        }
        float distanceTraveled = elapsedTime / timeBetweenPeaks;
        if(elapsedTime > timeBetweenPeaks) {
            distanceTraveled = 1f;
            upOrDown = false;
        } else if (elapsedTime < 0.0f) {
            distanceTraveled = 0.0f;
            upOrDown = true;
        }

        tf.position = Vector3.Lerp(startingPosition, endingPosition, distanceTraveled);
    }

    public virtual void OnTriggerEnter(Collider other ) {
        GameManager.am.PlaySFX("Pickup", tf.position);
        Destroy(this.gameObject);
    }
}
