﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PickupJoker : Pickup {

    public PowerupJoker powerup;

    public override void OnTriggerEnter ( Collider other ) {
        PowerupManager powMan = other.GetComponent<PowerupManager>();
        if (powMan != null) {
            // Use the copy constructor here so that it's an entirely new copy of the original powerup.
            powMan.AddPowerup(new PowerupJoker(powerup));
            base.OnTriggerEnter(other);
        }
    }

}
