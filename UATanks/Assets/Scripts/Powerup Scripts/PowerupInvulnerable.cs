﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]

public class PowerupInvulnerable : Powerup {

    public PowerupInvulnerable(PowerupInvulnerable old) : base(old) {

    }

    public override void OnAddPowerup ( TankData data ) {
        data.health.invulnerable = true;
        base.OnAddPowerup(data);
    }

    public override void OnRemovePowerup ( TankData data ) {
        data.health.invulnerable = false;
        base.OnRemovePowerup(data);
    }

}
