﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class PowerupDamage : Powerup {

    public PowerupDamage ( PowerupDamage old ) : base(old) {
    }

    public override void OnAddPowerup(TankData data ) {
        data.shoot.damage *= bonusAmount;
        base.OnAddPowerup(data);
    }

    public override void OnRemovePowerup ( TankData data ) {
        data.shoot.damage /= bonusAmount;
        base.OnRemovePowerup(data);
    }
}
