﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PowerupManager : MonoBehaviour {

    public List<Powerup> activePowerups;
    private TankData tank;

	// Use this for initialization
	void Start () {
        activePowerups = new List<Powerup>();
        tank = GetComponent<TankData>();
	}

    // Update is called once per frame
    void Update () {
        for (int i = activePowerups.Count - 1; i >= 0; i--) {
            if (!activePowerups[i].isPermanent) {
                activePowerups[i].countdown -= Time.deltaTime;
                if (activePowerups[i].countdown <= 0) {
                    RemovePowerup(activePowerups[i]);
                }
            }
        }
    }

    public void AddPowerup(Powerup toAdd ) {
        activePowerups.Add(toAdd);
        toAdd.OnAddPowerup(tank);
    }

    public void RemovePowerup(Powerup toRemove ) {
        toRemove.OnRemovePowerup(tank);
        activePowerups.Remove(toRemove);
    }
}
