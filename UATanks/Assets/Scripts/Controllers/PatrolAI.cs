﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PatrolAI : AIController {

    [Header("Patrol AI Data")]
    [Tooltip("The amount the tank's speed is multiplied by if it is Alert or Attacking.")]
    [Range(1, 3)]
    public float alertSpeedMult;

    private bool alertModifier = false;
    

    public override void Idle () {
        if (alertStateExit) {
            pawn.forwardSpeed /= alertSpeedMult;
            alertModifier = false;
            alertStateExit = false;
        }
        // Make sure my waypoint is within range!!!
        currentWaypoint = Mathf.Clamp(currentWaypoint, 0, Waypoints.Count - 1);

        // Move towards waypoint
        MoveTowards(Waypoints[currentWaypoint].position);

        // If "close enough" switch to "next" waypoint
        if (Vector3.Distance(pawn.move.tf.position, Waypoints[currentWaypoint].position) <= waypointCutoff) {
            // Advance to next waypoint
            currentWaypoint++;

            // Deal with being at the LAST waypoint
            if (currentWaypoint >= Waypoints.Count) {
                currentWaypoint = 0;
            }
        }
    }

    public override void Alert () {
        if (alertStateEnter && !alertModifier) {
            pawn.forwardSpeed *= alertSpeedMult;
            alertModifier = true;
        }
        Idle();
    }

    public override void Attack () {
        if (alertStateEnter && !alertModifier) {
            pawn.forwardSpeed *= alertSpeedMult;
            alertModifier = true;
        }
        float angleToPlayer;
        Vector3 vectorToPlayer = playerTarget - pawn.move.tf.position;
        angleToPlayer = Vector3.Angle(pawn.move.tf.forward, vectorToPlayer);
        if (angleToPlayer < firingCone) {
            RaycastHit rayInfo;

            if (Physics.Raycast(pawn.move.tf.position, vectorToPlayer, out rayInfo, visionDistance)) {
                if (rayInfo.collider.gameObject.tag == "Player") {
                    pawn.shoot.Fire();
                }
            }
        }
        Idle();
    }

    public override void LowHealth () {
        if (alertStateExit) {
            pawn.forwardSpeed /= alertSpeedMult;
            alertModifier = false;
            alertStateExit = false;
        }
        // Make sure my waypoint is within range!!!
        currentWaypoint = Mathf.Clamp(currentWaypoint, 0, Waypoints.Count - 1);

        // Move towards waypoint
        MoveTowards(Waypoints[currentWaypoint].position);

        // If "close enough" switch to "next" waypoint
        if (Vector3.Distance(pawn.move.tf.position, Waypoints[currentWaypoint].position) <= waypointCutoff) {
            // Advance to next waypoint
            currentWaypoint--;

            // Deal with being at the LAST waypoint
            if (currentWaypoint <= 0) {
                currentWaypoint = Waypoints.Count - 1;
            }
        }

        base.LowHealth();
    }
}