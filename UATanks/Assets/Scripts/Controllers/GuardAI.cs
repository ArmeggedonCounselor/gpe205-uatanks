﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GuardAI : AIController {

    [Header("Guard Data")]
    [Tooltip("The transform of the place the AI is guarding.")]
    public Transform guardPost;
    [Tooltip("Maximum patrol distance from post in meters.")]
    [Range(5, 100)]
    public float patrolDistance;
    [Tooltip("Maximum distance in meters the AI will chase an enemy.")]
    [Range(15, 150)]
    public float chaseDistance;

    private float distanceToPost;

    protected override void Update () {

        distanceToPost = Vector3.Distance(pawn.move.tf.position, guardPost.position);
        base.Update();
    }

    public override void Idle () {
        if(distanceToPost > (patrolDistance / 2)) {
            MoveTowards(guardPost.position);
        }
    }

    public override void Alert () {
        if(distanceToPost < patrolDistance) {
            MoveTowards(alertTarget);
        }
    }

    public override void Attack () {
        float angleToPlayer;
        Vector3 vectorToPlayer = playerTarget - pawn.move.tf.position;
        angleToPlayer = Vector3.Angle(pawn.move.tf.forward, vectorToPlayer);
        if (angleToPlayer < firingCone) {
            RaycastHit rayInfo;

            if (Physics.Raycast(pawn.move.tf.position, vectorToPlayer, out rayInfo, visionDistance)) {
                if (rayInfo.collider.gameObject.tag == "Player") {
                    pawn.shoot.Fire();
                }
            }
        }
        else {
            pawn.move.TurnTowards(vectorToPlayer);
        }

        if(distanceToPost < chaseDistance) {
            MoveTowards(playerTarget);
        }
    }

    public override void LowHealth () {
        if (distanceToPost > (patrolDistance / 2)) {
            MoveTowards(guardPost.position);
        } else {
            float angleToPlayer;
            Vector3 playerVector = playerTarget - pawn.move.tf.position;
            angleToPlayer = Vector3.Angle(pawn.move.tf.forward, playerVector);
            if (angleToPlayer < firingCone) {
                RaycastHit rayInfo;

                if (Physics.Raycast(pawn.move.tf.position, playerVector, out rayInfo, visionDistance)) {
                    if (rayInfo.collider.gameObject.tag == "Player") {
                        pawn.shoot.Fire();
                    }
                }
            }
            else {
                pawn.move.TurnTowards(playerVector);
            }
        }
        base.LowHealth();
    }
}
