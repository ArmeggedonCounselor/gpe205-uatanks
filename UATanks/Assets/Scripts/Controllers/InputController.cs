﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputController : MonoBehaviour {

    private PlayerData pawn;

    [Header("Movement Keys")]
    public KeyCode forwardKey;
    public KeyCode reverseKey;
    public KeyCode turnLeft;
    public KeyCode turnRight;

    [Header("Fire Key")]
    public KeyCode fireKey;

	// Use this for initialization
	void Start () {
        pawn = GetComponent<PlayerData>();
	}
	
	// Update is called once per frame
	void Update () {
        if (Input.GetKey(forwardKey))
        {
            pawn.tank.move.Move(true);
        }
        if (Input.GetKey(reverseKey))
        {
            pawn.tank.move.Move(false);
        }
        if (Input.GetKey(turnLeft))
        {
            pawn.tank.move.Turn(false);
        }
        if (Input.GetKey(turnRight))
        {
            pawn.tank.move.Turn(true);
        }
        if (Input.GetKey(fireKey))
        {
            pawn.tank.shoot.Fire();
        }
        if (Input.GetKeyDown(KeyCode.Escape)) {
            GameManager.gm.GameOver();
        }
    }
}
