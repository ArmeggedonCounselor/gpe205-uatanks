﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AggressiveAI : AIController {

    [Header("Aggressive AI Data")]
    [Tooltip("The amount damage is multiplied by when the tank is at low health.")]
    [Range(1, 3)]
    public float lowHealthDamageMult;
    [Tooltip("The amount firerate is multiplied by when the tank is at low health.")]
    [Range(1, 3)]
    public float lowHealthFirerateMult;

    private bool lowHealthModifier = false;

    public override void Idle () {
        if (lowHealthExit) { // If the tank is leaving the Low Health state....
            pawn.shoot.damage /= lowHealthDamageMult;
            pawn.shoot.firerate /= lowHealthFirerateMult;
            lowHealthModifier = false;
            lowHealthExit = false;
        }
        // Make sure my waypoint is within range!!!
        currentWaypoint = Mathf.Clamp(currentWaypoint, 0, Waypoints.Count - 1);

        // Move towards waypoint
        MoveTowards(Waypoints[currentWaypoint].position);

        // If "close enough" switch to "next" waypoint
        if (Vector3.Distance(pawn.move.tf.position, Waypoints[currentWaypoint].position) <= waypointCutoff) {
            // Advance to next waypoint
            currentWaypoint++;

            // Deal with being at the LAST waypoint
            if (currentWaypoint >= Waypoints.Count) {
                currentWaypoint = 0;
            }
        }
    }

    public override void Alert () {
        if (lowHealthExit) {
            pawn.shoot.damage /= lowHealthDamageMult;
            pawn.shoot.firerate /= lowHealthFirerateMult;
            lowHealthModifier = false;
            lowHealthExit = false;
        }
        MoveTowards(alertTarget);
    }

    public override void Attack () {
        if (lowHealthExit) {
            pawn.shoot.damage /= lowHealthDamageMult;
            pawn.shoot.firerate /= lowHealthFirerateMult;
            lowHealthModifier = false;
            lowHealthExit = false;
        }
        float angleToPlayer;
        Vector3 vectorToPlayer = playerTarget - pawn.move.tf.position;
        angleToPlayer = Vector3.Angle(pawn.move.tf.forward, vectorToPlayer);
        if(angleToPlayer < firingCone) {
            RaycastHit rayInfo;

            if (Physics.Raycast(pawn.move.tf.position, vectorToPlayer, out rayInfo, visionDistance)) {
                if (rayInfo.collider.gameObject.tag == "Player") {
                    pawn.shoot.Fire();
                }
            }
        } else {
            pawn.move.TurnTowards(vectorToPlayer);
        }

        MoveTowards(playerTarget);
    }

    public override void LowHealth () {
        if (lowHealthEnter && !lowHealthModifier) {
            pawn.shoot.damage *= lowHealthDamageMult;
            pawn.shoot.firerate *= lowHealthFirerateMult;
            lowHealthModifier = true;
            lowHealthEnter = false;
        }
        if (sawPlayer) {
            Attack();
        }
        if(heardSound && !sawPlayer) {
            Alert();
        }
        if(!heardSound && !sawPlayer) {
            Idle();
        }
        base.LowHealth();
    }
}
