﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CowardAI : AIController {

    [Header("Coward AI Data")]
    [Tooltip("The amount the tank's speed is multiplied by when it has low health.")]
    [Range(1, 3)]
    public float lowHealthSpeedMult;

    private bool lowHealthModifier = false;

    public override void Idle () {
        if (lowHealthExit) {
            pawn.forwardSpeed /= lowHealthSpeedMult;
            lowHealthModifier = false;
            lowHealthExit = false;
        }

        // Make sure my waypoint is within range!!!
        currentWaypoint = Mathf.Clamp(currentWaypoint, 0, Waypoints.Count - 1);

        // Move towards waypoint
        MoveTowards(Waypoints[currentWaypoint].position);

        // If "close enough" switch to "next" waypoint
        if (Vector3.Distance(pawn.move.tf.position, Waypoints[currentWaypoint].position) <= waypointCutoff) {
            // Advance to next waypoint
            currentWaypoint++;

            // Deal with being at the LAST waypoint
            if (currentWaypoint >= Waypoints.Count) {
                currentWaypoint = 0;
            }
        }
    }

    public override void Alert () {
        if (lowHealthExit) {
            pawn.forwardSpeed /= lowHealthSpeedMult;
            lowHealthModifier = false;
            lowHealthExit = false;
        }

        Vector3 vectorToPlayer = alertTarget - pawn.move.tf.position;
        vectorToPlayer *= -1;
        vectorToPlayer.Normalize();
        vectorToPlayer *= pawn.forwardSpeed;
        MoveTowards(vectorToPlayer);
    }

    public override void Attack () {
        float angleToPlayer;
        Vector3 playerVector = playerTarget - pawn.move.tf.position;
        angleToPlayer = Vector3.Angle(pawn.move.tf.forward, playerVector);
        if (angleToPlayer < firingCone) {
            RaycastHit rayInfo;

            if (Physics.Raycast(pawn.move.tf.position, playerVector, out rayInfo, visionDistance)) {
                if (rayInfo.collider.gameObject.tag == "Player") {
                    pawn.shoot.Fire();
                }
            }
        } else {
            Alert();
        }
    }

    public override void LowHealth () {
        if (lowHealthEnter && !lowHealthModifier) {
            pawn.forwardSpeed *= lowHealthSpeedMult;
            lowHealthModifier = true;
        }
        Alert();
        base.LowHealth();
    }
}
