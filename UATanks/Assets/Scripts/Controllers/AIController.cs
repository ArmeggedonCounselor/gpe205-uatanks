﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AIController : MonoBehaviour {
    public enum State { Idle, Alert, Attack, LowHealth};
    public enum AvoidState { Normal, TurnToAvoid, MoveToAvoid };

    [Header("Patrol Data")]
    public List<Transform> Waypoints;
    public int currentWaypoint;
    public State currentState;
    public AvoidState avoidState = AvoidState.Normal;
    public float avoidStateDuration = 1f;
    public float alertStateDuration = 1f;
    public float attackStateDuration = 1f;
    public float waypointCutoff;
    public bool isPatrolling;

    [Header("AI Senses")]
    [Tooltip("The distance in meters that the AI can see.")]
    [Range(10, 1000)]
    public float visionDistance;
    [Tooltip("The distance in meters from which the AI avoids obstacles.")]
    [Range(0, 5)]
    public float avoidDistance;
    [Tooltip("The inner angle of the AI's vision cone in degrees.")]
    [Range(30, 160)]
    public float visionCone;
    [Tooltip("The angle at which the AI will attempt to fire on the player.")]
    [Range(5, 135)]
    public float firingCone;

    [Header("AI Data")]
    [Tooltip("The percentage of max health at which the AI will enter Low Health state.")]
    [Range(0,1)]
    public float lowHealthLimit;
    [Tooltip("The amount of health per second the AI heals while in Low Health state.")]
    [Range(0.5f, 5)]
    public float healAmount;
    // Pawn
    protected TankData pawn;
    // Timers
    private float avoidStateTime;
    private float alertStateTime;
    private float attackStateTime;
    // Targets for movement and attacking.
    protected Vector3 alertTarget;
    protected Vector3 playerTarget;
    // Flags for controlling states and other additional features.
    protected bool heardSound = false;
    protected bool sawPlayer = false;
    protected bool lowHealthEnter = false;
    protected bool lowHealthExit = false;
    protected bool attackStateEnter = false;
    protected bool alertStateEnter = false;
    protected bool alertStateExit = false;




    // Use this for initialization
    protected virtual void Start () {
        pawn = GetComponent<TankData>();
	}
	
	// Update is called once per frame
	protected virtual void Update () {
        seePlayer();
        ChangeState();
        switch (currentState) {
            case State.Idle:
                Idle();
                break;
            case State.Alert:
                Alert();
                break;
            case State.Attack:
                Attack();
                break;
            case State.LowHealth:
                LowHealth();
                break;
        }
        if(attackStateEnter && attackStateTime <= 0) {
            alertStateTime = alertStateDuration;
            attackStateEnter = false;
        }

        if(alertStateTime > 0) {
            alertStateTime -= Time.deltaTime;
        }
        if(attackStateTime > 0) {
            attackStateTime -= Time.deltaTime;
        }
        if (attackStateEnter && attackStateTime <= 0) {
            alertStateTime = alertStateDuration;
            attackStateEnter = false;
        }
    }

    virtual public void Idle () {
        // Nothing to put here for the parent class.
    }

    virtual public void Attack () {
        // Parent is blank.
       
    }

    virtual public void Alert () {
        // Nothing to put in here for the parent.
       
    }

    virtual public void LowHealth () {
        pawn.health.Heal(healAmount * Time.deltaTime); // All tanks heal over time when in Low Health state.
     }

    virtual public void MoveTowards(Vector3 target ) {
        if (avoidState == AvoidState.Normal) {
                // Make a new target
                // Uses the waypoint's X and Z coordinates, and the tank's Y, to avoid pointing up into the sky or down into the ground.
                Vector3 targetPosition = new Vector3(target.x, pawn.move.tf.position.y, target.z);
                // Find direction to next waypoint
                Vector3 dirToTarget = targetPosition - pawn.move.tf.position;
                // Turn towards the next waypoint
                pawn.move.TurnTowards(dirToTarget);

            if (CanMoveForward()) {
                // move forward
                pawn.move.Move(true);
            } else {
                avoidState = AvoidState.TurnToAvoid;
            }
        } else if(avoidState == AvoidState.TurnToAvoid) {
            pawn.move.Turn(true);

            if (CanMoveForward()) {
                avoidState = AvoidState.MoveToAvoid;
                avoidStateTime = Time.time;
            }
        } else if (avoidState == AvoidState.MoveToAvoid) {
            // If I can move forward, do so:
            if (CanMoveForward()) {
                pawn.move.Move(true);
            }
            // If I can't, TurnToAvoid:
            else {
                avoidState = AvoidState.TurnToAvoid;
            }
            // If I have moved forward for AvoidStateDuration seconds, then....
            if(Time.time >= avoidStateTime + avoidStateDuration) {
                avoidState = AvoidState.Normal;
            }
        }
    }

    bool CanMoveForward () {
        // If I can't move forward:
        if (Physics.Raycast(pawn.move.tf.position, pawn.move.tf.forward, avoidDistance)) {
            return false;
        }

        // If can move forward:
        return true;
    }

    // Rather than giving tanks a radius where they hear anything that moves, I made a NoiseMaker that does the actual work by calling this function on everything in a radius.
    // Future iterations might use some sort of "hearing multiplier" between 0 and 1 that allows some tanks to have better senses than others.
    public void hearSound (Vector3 target ) {
        alertTarget = target;
        heardSound = true;
    }

    // Simple seeing code - if the player is within the vision cone, check to see if you can cast a ray to them.
    // The GetClosestPlayer function in the Game Manager will make sure only the closest player is checked for.
    public void seePlayer () {
        float angleToPlayer;
        Vector3 targetPosition = GameManager.gm.GetClosestPlayer(pawn.move.tf.position);

        Vector3 vectorToPlayer = targetPosition - pawn.move.tf.position;
        angleToPlayer = Vector3.Angle(pawn.move.tf.forward, vectorToPlayer);

        if (angleToPlayer > visionCone) {
            if (sawPlayer) {
                sawPlayer = false;
                attackStateTime = attackStateDuration;
            }
            return;
        }

        RaycastHit rayInfo;

        if (Physics.Raycast(pawn.move.tf.position, vectorToPlayer, out rayInfo, visionDistance)) {
            if (rayInfo.collider.gameObject.tag == "Player") {
                playerTarget = targetPosition;
                attackStateEnter = true;
                sawPlayer = true;
                return;
            }
            else if (sawPlayer) {
                sawPlayer = false;
                attackStateTime = attackStateDuration;
            }
        }
    }

    // Since all tanks have the same states, and the same triggers for moving between states, change state stays in the parent.
    public void ChangeState () {
        if(pawn.health.healthRatio < lowHealthLimit) {
            currentState = State.LowHealth;
            if (!lowHealthEnter) {
                lowHealthEnter = true;
            }
            if (alertStateEnter) {
                alertStateEnter = false;
                alertStateExit = true;
            }
            return;
        }
        if (lowHealthEnter) {
            lowHealthEnter = false;
            lowHealthExit = true;
        }
        if ((!sawPlayer && !heardSound) && alertStateTime <= 0) {
            if (alertStateEnter) {
                alertStateEnter = false;
                alertStateExit = true;
            }
            alertStateTime = 0;
            currentState = State.Idle;
           
        }
        if (heardSound) {
            currentState = State.Alert;
            if (!alertStateEnter) {
                alertStateEnter = true;
            }
            heardSound = false;
            alertStateTime = alertStateDuration;
        }
        if(alertStateTime > 0) {
            currentState = State.Alert;
        }
        if(attackStateTime > 0) {
            currentState = State.Attack;
        }
        if (sawPlayer) {
            currentState = State.Attack;
            if (!alertStateEnter) {
                alertStateEnter = true;
            }
        }
    }

    public void AddWaypoint(Transform waypoint) {
        Waypoints.Add(waypoint);
    }

    public void AddWaypoint(List<Transform> waypoints ) {
        Waypoints.AddRange(waypoints);
    }
}
