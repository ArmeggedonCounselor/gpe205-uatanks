﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NoiseMaker : MonoBehaviour {
    [Tooltip("The distance in meters at which the noise can be heard.")]
    [Range(5, 50)]
    public float noiseRadius;

    private Transform tf;

    private void Start () {
        tf = GetComponent<Transform>();
    }

    public void MakeNoise () {
        int layerMask = 1 << 8;
        List<Collider> colls = new List<Collider>( Physics.OverlapSphere(tf.position, noiseRadius, layerMask));
        foreach (Collider coll in colls) {
            coll.BroadcastMessage("hearSound", tf.position, SendMessageOptions.DontRequireReceiver);
        }
    }
}
