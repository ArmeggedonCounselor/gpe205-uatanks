﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerData : MonoBehaviour {

    // This class just holds the score and a reference to the tank each player is controlling.

    public int score;
    public TankData tank;
    public Camera playCamera;
    public List<Image> lives;
    public Text scoreDisplay;
    public Slider lifeBar;

	// Use this for initialization
	void Start () {
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void MakeSplitScreen(int player ) {
        playCamera.rect = new Rect(GameManager.um.playertwocameraposition * player, 0, GameManager.um.multiplayerCameraWidth, 1);
    }

    public Rect GetCameraRect () {
        return playCamera.rect;
    }

    public void UpdateUI(int num ) {
        int i = 0;
        foreach (Image life in lives) {
            life.enabled = false;
            if(i < num) {
                life.enabled = true;
            }
            i++;
        }
        scoreDisplay.text = score.ToString();
        lifeBar.value = tank.health.healthRatio;
    }
}
