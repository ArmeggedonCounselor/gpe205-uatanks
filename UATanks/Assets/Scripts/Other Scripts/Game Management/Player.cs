﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour {

    public PlayerData target;
    public GameObject playerPrefab;
    public int lives;
    public float respawnTimer;
    public int score;
    public int playerNumber;
    public Transform spawnPoint;
    public Camera deathCamera;

    public Transform tf;

    // Use this for initialization
    void Start () {
        tf = GetComponent<Transform>();
	}
	
	// Update is called once per frame
	void Update () {
		if(target == null && respawnTimer > 0 && lives > -1) {
            respawnTimer -= Time.deltaTime;
            if(respawnTimer <= 0) {
                SpawnPlayer();
            }
        }
        if(lives == -1) {
            GameManager.gm.deadPlayers++;
            lives--;
        }
	}

    void LateUpdate () {
        if (target != null) {
            tf.position = target.tank.move.tf.position;
            UpdateUI();
        }
    }

    public void SpawnPlayer () {
        int spawn = Random.Range(0, GameManager.gm.PlayerSpawns.Count);
        GameObject tempPlayer;
        spawnPoint = GameManager.gm.PlayerSpawns[spawn];
        if (deathCamera.isActiveAndEnabled) {
            deathCamera.enabled = false;
        }
        tempPlayer = Instantiate(playerPrefab, spawnPoint.position, spawnPoint.rotation) as GameObject;
        target = tempPlayer.GetComponent<PlayerData>();
        target.score += score;
        if(GameManager.gm.numberOfPlayers == 2) {
            makeSplitScreen();
        }
    }

    public void makeSplitScreen () {
        target.MakeSplitScreen(playerNumber);
        deathCamera.rect = target.GetCameraRect();
    }

    public void UpdateUI () {
        target.UpdateUI(lives);
    }

    public void ActivateDeathCam () {
        deathCamera.enabled = true;
    }
}
