﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameOverUI : MonoBehaviour {

    public Text highScores;
    public InputField playerOneScore;
    public int playerNumber = 0;

    public void Start () {
        playerNumber = 0;
    }


    public void SetName(Text input) {
        GameManager.gm.currentScores[playerNumber].name = input.text;
        playerNumber++;
    }

    public void DisplayHighScores () {
        GameManager.gm.saveHighScores();
        playerOneScore.enabled = false;

        highScores.text = "*****HIGH SCORES*****\n\n";
        for(int i = 0; i < GameManager.gm.highScores.Count; i++) {
            highScores.text += (i + 1).ToString() + ". " + GameManager.gm.highScores[i].name + "   " + GameManager.gm.highScores[i].score.ToString() + "\n";
        }
    }
}
