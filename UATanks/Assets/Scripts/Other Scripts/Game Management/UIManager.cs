﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIManager : MonoBehaviour {

    public float multiplayerCameraWidth = 0.499f;
    public float playertwocameraposition = 0.501f;

    private void Awake () {
        if(GameManager.um != null) {
            Destroy(this.gameObject);
        }
        GameManager.um = this;
    }

    public void setBGM (Slider input) {
        GameManager.am.bgmVolume = input.value;
    }

    public void setSFX (Slider input) {
        GameManager.am.sfxVolume = input.value;
    }

    public void setSeed(Text input ) {
        GameManager.mapGen.SetSeed(input.text);
    }

    public void toggleMapOTD (Toggle trigger) {
        GameManager.mapGen.mapOfTheDay = trigger.isOn;
    }

    public void toggleCustomSeed (Toggle trigger) {
        GameManager.mapGen.useCustomSeed = trigger.isOn;
    }

	public void SetPlayers(int players ) {
        GameManager.gm.SetPlayers(players);
    }

    public void OnClick () {
        GameManager.am.PlaySFX("Click", Camera.main.transform.position);
    }

    public void MouseOver () {
        GameManager.am.PlaySFX("MouseOver", Camera.main.transform.position);
    }
}
