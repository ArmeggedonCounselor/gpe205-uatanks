﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneLoader : MonoBehaviour {

    private void Awake () {
        if(GameManager.sceneLoader != null) {
            Destroy(this.gameObject);
        }
        GameManager.sceneLoader = this;
    }

    /// <summary>
    /// Loads a scene.
    /// </summary>
    /// <param name="name"> The name of the scene to be loaded.</param>
	public void Load (string name ) {
        SceneManager.LoadScene(name);
    }

    /// <summary>
    /// Loads a scene.
    /// </summary>
    /// <param name="number">The Load Order position of the scene.</param>
    public void Load (int number ) {
        SceneManager.LoadScene(number);
    }

    /// <summary>
    /// Gets the current scene's name.
    /// </summary>
    /// <returns>The name of the current scene.</returns>
    public string CurrentScene () {
        return SceneManager.GetActiveScene().name;
    }
}
