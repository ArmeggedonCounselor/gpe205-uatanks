﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

[Serializable]
public class ScoreEntry : IComparable<ScoreEntry> {
    public string name;
    public int score;

    public int CompareTo(ScoreEntry other ) {
        int returnValue = 0;
        if(other == null) {
            returnValue = -1;
        }
        if(this.score > other.score) {
            returnValue = -1;
        }
        if(this.score < other.score) {
            returnValue = 1;
        }

        return returnValue;
    }
}


public class GameManager : MonoBehaviour {

    public static GameManager gm;
    public static MapGenerator mapGen;
    public static SceneLoader sceneLoader;
    public static AudioManager am;
    public static UIManager um;
    public static SaveManager sm;
    

    public int maxLives = 3;
    public GameObject playerPrefab;
    public GameObject playerTwoPrefab;
    public float respawnTimer;
    public GameObject playerContainerPrefab;

    // These will be private in the final version, but they are public now for this release.
    private List<TankData> EnemyTanks;
    [HideInInspector]public List<Transform> PlayerSpawns;
    private List<Player> Players;
    [HideInInspector]public List<ScoreEntry> currentScores;
    [HideInInspector]public List<ScoreEntry> highScores;
    private List<GameObject> spawnedPowerups;
    public int numberOfPlayers;
    [HideInInspector]public int deadPlayers;

    void Awake () {
        if (gm != null) {
            Debug.Log("There can only be one game manager.");
            Destroy(this.gameObject);
        }
        gm = this;
        DontDestroyOnLoad(this.gameObject);
    }

    // Use this for initialization
    void Start () {
        EnemyTanks = new List<TankData>();
        PlayerSpawns = new List<Transform>();
        Players = new List<Player>();
        spawnedPowerups = new List<GameObject>();
        currentScores = new List<ScoreEntry>();
        highScores = new List<ScoreEntry>();
        sm.Load();
        am.PlayMusic("Menu");
    }

    void OnEnable () {
        SceneManager.sceneLoaded += OnSceneLoaded;
    }

    void OnSceneLoaded (Scene scene, LoadSceneMode mode) {
        switch (scene.name) {
            case "Main Menu": am.PlayMusic("Menu");
                currentScores.Clear();
                break;
            case "Game": am.PlayMusic("Game");
                deadPlayers = 0;
                mapGen.BuildMap();
                for(int i = 0; i < numberOfPlayers; i++) {
                    Player newPlayer = Instantiate(playerContainerPrefab).GetComponent<Player>();
                    newPlayer.lives = maxLives;
                    newPlayer.respawnTimer = 0;
                    newPlayer.score = 0;
                    newPlayer.playerNumber = i;
                    if(i == 1) {
                        newPlayer.playerPrefab = playerTwoPrefab;
                    } else {
                        newPlayer.playerPrefab = playerPrefab;
                    }
                    newPlayer.SpawnPlayer();
                    Players.Add(newPlayer);
                }
                break;
            case "Options": am.PlayMusic("Menu");
                break;
            case "Game Over": am.PlayMusic("Menu");
                mapGen.ClearMap();
                EnemyTanks.Clear();
                PlayerSpawns.Clear();
                Players.Clear();
                spawnedPowerups.Clear();
                Cursor.lockState = CursorLockMode.None;
                Cursor.visible = true;
                break;
        }
    }

    // Update is called once per frame
    void Update () {
       if(sceneLoader.CurrentScene() == "Game") {
            if(deadPlayers == numberOfPlayers) {
                GameOver();
            }
        }
    }

    public void SetPlayers(int players){
        numberOfPlayers = players;
    } 

    public void GameOver () {
        SaveScores();
        sceneLoader.Load("Game Over");
    }

    public void Exit () {
        sm.Save();
        Application.Quit();
    }

    public void SaveScores () {
        foreach(Player player in Players) {
            ScoreEntry newScore = new ScoreEntry();
            newScore.score = player.score;
            newScore.name = player.playerNumber.ToString();
            currentScores.Add(newScore);
        }
    }

    public void saveHighScores () {
        if (currentScores.Count > 0) {
            highScores.AddRange(currentScores);
        }
        highScores.Sort();
        if(highScores.Count > 10) {
            highScores.RemoveRange(10, highScores.Count - 10);
        }
    }

    /// <summary>
    /// Adds "score" to target's score.
    /// </summary>
    /// <param name="target"></param>
    /// <param name="score"></param>
    public void AddScore ( GameObject target, int adjustScore ) {
        TankData shooter = target.GetComponent<TankData>();
        if (shooter == null) { return; }
        for (int i = 0; i < Players.Count; i++) {
            if (Players[i].target != null) {
                if (Players[i].target.tank == shooter) {
                    Players[i].target.score += adjustScore;
                    Players[i].score += adjustScore;
                }
            }
        }
    }

    /// <summary>
    /// Kills the target (and actually removes it from the Lists to avoid accidental null issues.)
    /// </summary>
    /// <param name="target"></param>
    public void Kill ( GameObject target ) {
        TankData tankData = target.GetComponent<TankData>();
        if(tankData == null) {
            Destroy(target);
            return;
        }
        foreach(TankData t in EnemyTanks) {
            if(tankData == t) {
                EnemyTanks.Remove(t);
                Destroy(target);
                return;
            }
        }
        for(int i = 0; i < Players.Count; i++) {
            if (Players[i].target != null) {
                if (Players[i].target.tank == tankData) {
                    Players[i].lives--;
                    Players[i].respawnTimer = respawnTimer;
                    Players[i].target = null;
                    Players[i].ActivateDeathCam();
                    Destroy(target.gameObject);
                    return;
                }
            }
        }
        Debug.Log("Somehow, something that wasn't a real tank called Kill. Offending Object: " + target.name);
        Destroy(target);
    }

    /// <summary>
    /// Gets the closest player.
    /// </summary>
    /// <param name="origin"> The position from where the search starts. </param>
    /// <returns></returns>
    public Vector3 GetClosestPlayer(Vector3 origin ) {
        Vector3 target = new Vector3(0, 50000, 0);
        foreach(Player player in Players) {
            if(Vector3.Distance(target, origin) >= Vector3.Distance(player.tf.position, origin)) {
                target = player.tf.position;
            }
        }
        return target;
    }

    /// <summary>
    /// Add an enemy to the list.
    /// </summary>
    /// <param name="toAdd"> The TankData component of the enemy being added.</param>
    public void AddEnemy (TankData toAdd ) {
        EnemyTanks.Add(toAdd);
    }

    /// <summary>
    /// Add a point where a player can spawn to the list.
    /// </summary>
    /// <param name="playerSpawn"> The transform of the spawn point. </param>
    public void AddPlayerSpawn ( Transform playerSpawn ) {
        PlayerSpawns.Add(playerSpawn);
    }

    public void AddPowerup(GameObject powerup ) {
        spawnedPowerups.Add(powerup);
    }
}
