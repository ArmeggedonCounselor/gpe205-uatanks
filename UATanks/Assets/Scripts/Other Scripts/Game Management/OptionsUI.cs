﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class OptionsUI : MonoBehaviour {

    public Toggle MapOfTheDay;
    public Toggle CustomSeed;
    public InputField CustomSeedInput;
    public Slider SFXSlider;
    public Slider BGMSlider;

    public void Start() {
        MapOfTheDay.isOn = GameManager.mapGen.mapOfTheDay;
        CustomSeed.isOn = GameManager.mapGen.useCustomSeed;
        string text = GameManager.mapGen.seed.ToString();
        if (text != "0") {
            CustomSeedInput.text = text;
        }
        SFXSlider.value = GameManager.am.sfxVolume;
        BGMSlider.value = GameManager.am.bgmVolume;
         
    }

	
}
