﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class MapGenerator : MonoBehaviour {

    public List<Room> rooms;
    public float tileXWidth;
    public float tileZWidth;

    public int numCols;
    public int numRows;

    public List<GameObject> tilePrefabs;

    public int seed;
    public bool mapOfTheDay;
    public bool useCustomSeed;

    private bool seedSet = false;


    private void Awake () {
        if(GameManager.mapGen != null) {
            Destroy(this.gameObject);
        }
        GameManager.mapGen = this;
    }
    // Use this for initialization
    void Start () {
        rooms = new List<Room>();
	}
	// Update is called once per frame
	void Update () {
		
	}

    public void BuildMap () {
        if (!seedSet) {
            SetSeed("");
        }
        for(int row = 0; row < numRows; row++) {
            for(int col = 0; col < numCols; col++) {

                // Generate a random tile
                int rand = UnityEngine.Random.Range(0, tilePrefabs.Count);
                GameObject newTile = Instantiate(tilePrefabs[rand]) as GameObject;
                // Name it
                newTile.name = "Tile_" + col + "_" + row;
                // Move it into position.
                float xPos = col * tileXWidth;
                float zPos = row * -tileZWidth;
                newTile.transform.position = new Vector3(xPos, 0, zPos);
                rooms.Add(newTile.GetComponent<Room>());
            }
        }
        for(int row = 0; row < numRows; row++) {
            for (int col = 0; col < numCols; col++) {
                Room currentRoom = rooms[GetIndex(row, col)];
                Room[] neighbors = { currentRoom, currentRoom, currentRoom, currentRoom };                
                // Switching on outside walls.
                if (numRows > 1) {
                    if (row == 0) {
                        currentRoom.wallNorth.SetActive(true);
                        neighbors[0] = null;
                        //south = baseMap[GetIndex(row + 1, col)];
                    }
                    else if (row == numRows - 1) {
                        currentRoom.wallSouth.SetActive(true);
                        neighbors[2] = null;
                        //north = baseMap[GetIndex(row - 1, col)];
                    }
                } else {
                    currentRoom.wallNorth.SetActive(true);
                    currentRoom.wallSouth.SetActive(true);
                    neighbors[0] = null;
                    neighbors[2] = null;
                }
                if (numCols > 1) {
                    if (col == 0) {
                        currentRoom.wallWest.SetActive(true);
                        neighbors[3] = null;
                        //east = baseMap[GetIndex(row, col + 1)];
                    }
                    else if (col == numCols - 1) {
                        currentRoom.wallEast.SetActive(true);
                        neighbors[1] = null;
                        //west = baseMap[GetIndex(row, col - 1)];
                    }
                } else {
                    currentRoom.wallWest.SetActive(true);
                    currentRoom.wallEast.SetActive(true);
                    neighbors[1] = null;
                    neighbors[3] = null;
                }
                for(int x = 0; x < neighbors.Length; x++) {
                    if(neighbors[x] != null) {
                        switch (x) {
                            case 0: neighbors[0] = rooms[GetIndex(row - 1, col)];
                                break;
                            case 1: neighbors[1] = rooms[GetIndex(row, col + 1)];
                                break;
                            case 2: neighbors[2] = rooms[GetIndex(row + 1, col)];
                                break;
                            case 3: neighbors[3] = rooms[GetIndex(row, col - 1)];
                                break;
                        }
                    }
                }
                // We only need to place walls if a tile is indoors.
                if (!currentRoom.isOutdoor) {
                    for (int x = 0; x < neighbors.Length; x++) {
                        if (neighbors[x] != null) { // To avoid issues, check if there is actually a neighbor in that direction.
                            Room neighboringRoom = neighbors[x]; // Get the room - simplifies code a bit.
                            if (neighboringRoom.isOutdoor) { // If the neighboring tile is outdoors, place an open wall to it.
                                switch (x) {
                                    case 0:
                                        currentRoom.halfWallEastNorth.SetActive(true);
                                        currentRoom.halfWallWestNorth.SetActive(true);
                                        currentRoom.connectionNorth = true;
                                        break;
                                    case 1:
                                        currentRoom.halfWallNorthEast.SetActive(true);
                                        currentRoom.halfWallSouthEast.SetActive(true);
                                        currentRoom.connectionEast = true;
                                        break;
                                    case 2:
                                        currentRoom.halfWallEastSouth.SetActive(true);
                                        currentRoom.halfWallWestSouth.SetActive(true);
                                        currentRoom.connectionSouth = true;
                                        break;
                                    case 3:
                                        currentRoom.halfWallNorthWest.SetActive(true);
                                        currentRoom.halfWallSouthWest.SetActive(true);
                                        currentRoom.connectionWest = true;
                                        break;
                                }
                            }
                            else { // If the neighboring room is indoors,
                                int makeDoor = UnityEngine.Random.Range(0, 2); // Flip a coin...
                                switch (x) {
                                    case 0:
                                        currentRoom.connectionNorth = neighbors[x].connectionSouth;
                                        break;
                                    case 1:
                                        if (makeDoor == 1) {
                                            currentRoom.wallEast.SetActive(false);
                                            currentRoom.halfWallNorthEast.SetActive(true);
                                            currentRoom.halfWallSouthEast.SetActive(true);
                                            currentRoom.connectionEast = true;
                                        }
                                        else {
                                            currentRoom.wallEast.SetActive(true);
                                            currentRoom.connectionEast = false;
                                        }
                                        break;
                                    case 2:
                                        if (makeDoor == 1) {
                                            currentRoom.wallSouth.SetActive(false);
                                            currentRoom.halfWallEastSouth.SetActive(true);
                                            currentRoom.halfWallWestSouth.SetActive(true);
                                            currentRoom.connectionSouth = true;
                                        }
                                        else {
                                            currentRoom.wallSouth.SetActive(true);
                                            currentRoom.connectionSouth = false;
                                        }
                                        break;
                                    case 3:
                                        currentRoom.connectionWest = neighbors[x].connectionEast;
                                        break;
                                }
                            }
                        }
                    }
                    // If we somehow end up with a room with no connections, we will just go back and repeat this whole process on that room.
                    // This is a pretty dirty way of doing things, and will definitely lead to larger chains of connected interiors, but should prevent weird cases where
                    // We have a chain of of connected rooms that don't connect to an exterior tile.
                    if (!currentRoom.connectionEast && !currentRoom.connectionNorth && !currentRoom.connectionSouth && !currentRoom.connectionWest) {
                        Debug.Log("Room at " + row + ", " + col + " had no connections.");
                        if (row < numRows - 1 && col < numCols - 1) {
                            col -= 1;
                        }
                        else {
                            if (neighbors[0] != null) {
                                if (!neighbors[0].connectionSouth) {
                                    neighbors[0].wallSouth.SetActive(false);
                                    neighbors[0].halfWallEastSouth.SetActive(true);
                                    neighbors[0].halfWallWestSouth.SetActive(true);
                                    neighbors[0].connectionSouth = true;
                                }
                            }
                        }
                    }
                }
                GameManager.gm.AddPlayerSpawn(currentRoom.playerSpawn);
                
            }
        }
    }

    public int DateToInt(DateTime date ) {
        return date.Year + date.Month + date.Day + date.Hour + date.Minute + date.Second + date.Millisecond;
    }

    public int GetIndex(int x, int y ) {
        return x * numCols + y;
    }

    public void SetSeed(string input ) {
        if (!useCustomSeed) {
            if (!mapOfTheDay) {
                seed = DateToInt(DateTime.Now);
            }
            else {
                seed = DateToInt(DateTime.Today);
            }
        } else {
            int newSeed;
            if (int.TryParse(input, out newSeed)) {
                seed = newSeed;
            } else {
                seed = DateToInt(DateTime.Now);
            }
        }
        UnityEngine.Random.InitState(seed);
        seedSet = true;
    }

    public void ClearMap () {
        rooms.Clear();
    }
}