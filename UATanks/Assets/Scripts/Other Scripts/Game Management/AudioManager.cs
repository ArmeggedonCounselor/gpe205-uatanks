﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioManager : MonoBehaviour {

    [Header("Audio Clips")]
    public List<AudioClip> SFXclips;
    public List<AudioClip> BGMclips;

    [Header("Volume")]
    [Range(0,1)]public float bgmVolume;
    [Range(0,1)]public float sfxVolume;

    public AudioSource BGMSource;

    public void Awake () {
        if(GameManager.am != null) {
            Destroy(this.gameObject);
        }
        GameManager.am = this;
    }

    public void Start () {
        BGMSource = GetComponent<AudioSource>();
    }

    public void Update () {
        if(bgmVolume != BGMSource.volume) {
            BGMSource.volume = bgmVolume;
        }
    }

    // This function finds the named audio clip in the BGM clips List.
    // Why did I make this, rather than simply going through the loop in the only function that calls this function?
    // *shrugs*
    private AudioClip FindClip(string name ) {
        foreach(AudioClip clip in BGMclips) {
            if(clip.name == name) {
                return clip;
            }
        }
        return null;
    }

    /// <summary>
    /// Plays a named sound effect at a specified location.
    /// </summary>
    /// <param name="name">The name of the sound effect to be played.</param>
    /// <param name="location">The Vector3 location where the sound effect will play.</param>
    public void PlaySFX(string name, Vector3 location ) {
        AudioClip clip = null;
        foreach(AudioClip c in SFXclips) {
            if(c.name == name) {
                clip = c;
                break;
            }
        }
        if (clip != null) {
            AudioSource.PlayClipAtPoint(clip, location, sfxVolume);
        } else {
            Debug.Log("Cannot find AudioClip \"" +name +"\".");
        }
    }

    /// <summary>
    /// Sets the background music to the named audioclip.
    /// </summary>
    /// <param name="name">The name of the audio clip to be played as background music.</param>
    public void PlayMusic(string name ) {
        AudioClip clip = FindClip(name);
        if (clip != null) {
            if (BGMSource.isPlaying) {
                if (clip != BGMSource.clip) {
                    BGMSource.clip = clip;
                    BGMSource.volume = bgmVolume;
                    BGMSource.loop = true;
                    BGMSource.Play();
                }
            } else {
                BGMSource.clip = clip;
                BGMSource.volume = bgmVolume;
                BGMSource.loop = true;
                BGMSource.Play();
            }
        }
        else {
            Debug.Log("Cannot find AudioClip \"" + name + "\".");
        }
    }	
}
