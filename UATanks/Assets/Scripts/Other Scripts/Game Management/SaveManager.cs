﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SaveManager : MonoBehaviour {

    public void Awake () {
        if(GameManager.sm != null) {
            Destroy(this.gameObject);
        }
        GameManager.sm = this;
    }

	public void Save () {
        PlayerPrefs.SetFloat("BGM Volume", GameManager.am.bgmVolume);
        PlayerPrefs.SetFloat("SFX Volume", GameManager.am.sfxVolume);
        PlayerPrefs.SetInt("Map Seed", GameManager.mapGen.seed);
        PlayerPrefs.SetString("Use MapOTD", GameManager.mapGen.mapOfTheDay.ToString());
        PlayerPrefs.SetString("Use Custom Seed", GameManager.mapGen.useCustomSeed.ToString());
        for(int i = 0; i < GameManager.gm.highScores.Count; i++) {
            string json = JsonUtility.ToJson(GameManager.gm.highScores[i]);
            PlayerPrefs.SetString("High Score " + i.ToString(), json);
            
        }
        PlayerPrefs.Save();
    }

    public void Load () {
        int i = 0;
        while (PlayerPrefs.HasKey("High Score " + i.ToString())) {
            string score = PlayerPrefs.GetString("High Score " + i.ToString());
            ScoreEntry entry = JsonUtility.FromJson<ScoreEntry>(score);
            GameManager.gm.highScores.Add(entry);
            i++;
        }
        string customSeed = PlayerPrefs.GetString("Use Custom Seed", "False");
        switch (customSeed) {
            case "True":
                GameManager.mapGen.useCustomSeed = true;
                break;
            case "False":
                GameManager.mapGen.useCustomSeed = false;
                break;
        }
        string mapOfTheDay = PlayerPrefs.GetString("Use MapOTD", "False");
        switch (mapOfTheDay) {
            case "True":
                GameManager.mapGen.mapOfTheDay = true;
                break;
            case "False":
                GameManager.mapGen.mapOfTheDay = false;
                break;
        }
        GameManager.mapGen.SetSeed(PlayerPrefs.GetInt("Map Seed", 0).ToString());
        GameManager.am.sfxVolume = PlayerPrefs.GetFloat("SFX Volume", 1.0f);
        GameManager.am.bgmVolume = PlayerPrefs.GetFloat("BGM Volume", 1.0f);
    }

    public void ClearHighScores () {
        for(int i = 0; i < GameManager.gm.highScores.Count; i++) {
            PlayerPrefs.DeleteKey("High Score " + i.ToString());
        }
        GameManager.gm.highScores.Clear();
    }
}