﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour {

   
    public GameObject owner;
    public ParticleSystem explosion;
 
    public float damage;

	// Use this for initialization
	void Start () {
	}

    // Need to track this, but can't instantiate with two variables unless I want to create a new class just for that.
    // So, created this to set the variable. Also means I can make all the variables private, since they're just passed out in the collision.
    public void SetOwner(GameObject newOwner)
    {
        owner = newOwner;
    }

    // Created this to set the variable.
    public void SetDamage(float dam)
    {
        damage = dam;
    }

    // Does damage to any object that has a health component. Also, destroys the bullet.
    void OnCollisionEnter(Collision coll)
    {
        TankHealth health = coll.collider.gameObject.GetComponent<TankHealth>();
        if(health != null)
        {
            health.TakeDamage(damage, owner);
        } else {
            GameManager.am.PlaySFX("HitWall", GetComponent<Transform>().position);
        }
        if(explosion.gameObject.activeInHierarchy == false) {
            explosion.gameObject.SetActive(true);
        }
        explosion.Play();
        Destroy(this.gameObject);
    }
}
