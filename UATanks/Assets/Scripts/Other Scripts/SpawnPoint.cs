﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnPoint : MonoBehaviour {

    public GameObject objectToSpawn;
    [HideInInspector]public GameObject spawnedObject;
    private Transform tf;
    public Room room;
    public float timeBetweenSpawns;
    private float spawnCountdown;

	// Use this for initialization
	void Start () {
        tf = GetComponent<Transform>();
        spawnCountdown = 1;
	}
	
	// Update is called once per frame
	void Update () {
		if(spawnedObject == null) {
            spawnCountdown -= Time.deltaTime;
            if(spawnCountdown <= 0) {
                spawnedObject = Instantiate(objectToSpawn, tf.position, tf.rotation) as GameObject;
                GuardAI guard = spawnedObject.GetComponent<GuardAI>();
                if (guard != null) {
                    guard.guardPost = room.guardPost;
                }
                else if (guard == null) {
                    AIController tankAI = spawnedObject.GetComponent<AIController>();
                    tankAI.AddWaypoint(room.patrolPoints);
                }
                GameManager.gm.AddEnemy(spawnedObject.GetComponent<TankData>());
                spawnCountdown = timeBetweenSpawns;
            }
        }
	}
}
