﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TankShooter : MonoBehaviour {

    [Tooltip("Shots per second")] public float firerate = 1f;
    public float damage;
    public float timeToRemove;
    public float fireForce;

    public GameObject bulletPrefab;
    public Transform firePoint;
    public ParticleSystem muzzleFlash;

    private bool canFire = true;
    private float nextShotAtTime;
    private float secondsPerShot;

    void Start () {
        nextShotAtTime = 0f;
       // Translating from shots per second to seconds per shot.
    }

    // Timers! Firerate is exposed to designers, and is in shots per second, but it gets translated straight into a more useful form for these timers.
    void Update () {
        secondsPerShot = 1 / firerate;
        if (nextShotAtTime > 0) {
            nextShotAtTime -= Time.deltaTime;
        }
        if (nextShotAtTime <= 0) {
            canFire = true;
        }
    }

    // Fires the boolet.
    public void Fire () {
        if (canFire) {
            GameObject newBullet = Instantiate(bulletPrefab, firePoint.position, firePoint.rotation) as GameObject;
            GameManager.am.PlaySFX("Fire", firePoint.position);
            if(muzzleFlash.gameObject.activeInHierarchy == false) {
                muzzleFlash.gameObject.SetActive(true);
            }
            muzzleFlash.Play();
            Rigidbody rb = newBullet.GetComponent<Rigidbody>();
            newBullet.BroadcastMessage("SetOwner", this.gameObject);
            newBullet.BroadcastMessage("SetDamage", damage);
            rb.AddForce(newBullet.transform.forward * fireForce);
            Destroy(newBullet, timeToRemove);
            nextShotAtTime = secondsPerShot;
            canFire = false;
        }
    }
}
