﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TankHealth : MonoBehaviour {

    public float maxHealth;
    public float currentHealth;
    public float healthRatio;
    public bool invulnerable = false;
    public ParticleSystem deathExplosion;

    TankData data;
    GameObject lastShooter;

	// Use this for initialization
	void Start () {
        data = GetComponent<TankData>();
        currentHealth = maxHealth;
	}

    // OLD: Update used solely to check if the tank is currently dead, and reacting accordingly.
    // NEW: Update is also used to keep the healthRatio up-to-date.
    void Update()
    {
        healthRatio = currentHealth / maxHealth;
        if(currentHealth <= 0)
        {
            GameManager.gm.AddScore(lastShooter, data.pointValue);
            GameManager.am.PlaySFX("Death", data.move.tf.position);
            if(deathExplosion.gameObject.activeInHierarchy == false) {
                deathExplosion.gameObject.SetActive(true);
            }
            deathExplosion.Play();
            GameManager.gm.Kill(this.gameObject);
        }
    }

    // To properly assign the score, I have to track who shot this tank. Might refactor to pass a TankData variable instead of a GameObject, but that's less important, more for optimization.
    public void TakeDamage(float damage, GameObject shooter)
    {
        if (invulnerable) {
            GameManager.am.PlaySFX("HitWall", data.move.tf.position);
            return;
        }
        GameManager.am.PlaySFX("TankHit", data.move.tf.position);
        currentHealth -= damage;
        lastShooter = shooter;
    }

    public void Heal(float damage ) {
        currentHealth += damage;
    }

    //In case I need it in the future:
    //public void DamageOverTime(float damage)
    //Might add a flamethrower weapon or an acid splash, something that does damage over time rather than all at once.
}
