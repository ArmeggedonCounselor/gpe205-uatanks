﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(TankMover))]
[RequireComponent(typeof(TankShooter))]
[RequireComponent(typeof(TankHealth))]

public class TankData : MonoBehaviour {

    // This class is just a container for data and stuff.

	[Header("Data")]
	public float forwardSpeed;
	public float reverseSpeed;
	public float turnSpeed;
    public int pointValue;

	// Tank Components
	[HideInInspector]public TankMover move;
	[HideInInspector]public TankShooter shoot;
    [HideInInspector]public TankHealth health;

	void Start () {
		move = GetComponent<TankMover> ();
		shoot = GetComponent<TankShooter> ();
        health = GetComponent<TankHealth>();
	}


}
