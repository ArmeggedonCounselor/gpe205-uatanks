﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TankMover : MonoBehaviour {

    private CharacterController controller;
    [HideInInspector] public Transform tf;
    private TankData data;
    private NoiseMaker noise;

    // Use this for initialization
    void Start () {
        controller = GetComponent<CharacterController>();
        tf = GetComponent<Transform>();
        data = GetComponent<TankData>();
        noise = GetComponent<NoiseMaker>();
    }

    // Update is called once per frame
    void Update () {

    }

    /// <summary>
    /// Uses SimpleMove to move. Takes a bool which is true if the tank is moving forward, and false if it's moving in reverse.
    /// </summary>
    /// <param name="frontOrBack"></param>
	public void Move ( bool frontOrBack ) {
        // Move on the moveVector
        if (frontOrBack) {
            controller.SimpleMove(tf.forward.normalized * data.forwardSpeed);
        }
        else {
            controller.SimpleMove(tf.forward.normalized * -1 * data.reverseSpeed);
        }
        if(noise != null) {
            noise.MakeNoise();
        }
    }

    /// <summary>
    /// Turns the tank. Takes a bool which is true if the tank is turning right and false if it's turning left.
    /// </summary>
    /// <param name="direction"></param>
	public void Turn ( bool direction ) {
        float dir;
        if (direction) {
            dir = 1f;
        }
        else {
            dir = -1f;
        }
        // Turn in the direction of turnSpeedAndDirection
        tf.Rotate(0, dir * data.turnSpeed * Time.deltaTime, 0);
    }

    public void TurnTowards ( Vector3 targetDirection ) {
        // Find the rotation that looks down the vector "targetDirection"
        float angle = Vector3.Angle(tf.forward, targetDirection);
        tf.Rotate(new Vector3(0,1,0), angle * data.turnSpeed * Time.deltaTime);
    }
}
